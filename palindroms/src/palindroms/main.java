package palindroms;

public class main {

    public static void main(String[] args) {

        String str = "Asa";
        System.out.println(IsPalindrom.isPalindrom(str));
        System.out.println(IsPalindrom.reversString(str));

    }
}
